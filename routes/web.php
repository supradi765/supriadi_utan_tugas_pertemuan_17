<?php
use App\Http\Controllers\DelmanController;
use App\Http\Controllers\ItemController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::controller(ItemController::class)->name('item.')->prefix('master-data/item')->group(function () {
    Route::get('/'
    ,
    'item')->name('item');
    });
 Route::controller(DelmanController::class)->name('delman.')->prefix('master-data/delman')->group(function () {
        Route::get('/'
        ,
        'delman')->name('delman');
        });
        
