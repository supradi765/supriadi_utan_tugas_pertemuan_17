<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('style2.css') }}">

</head>
<body>
    <nav class="nav">
        <div class="nav-kiri">
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">contact</a></li>
            <li><a href="#">about</a></li>
        </ul>
        </div>
        <div class="nav-kanan">
            <button onclick="Pesan('silahkan login')">login</button>
            <button onclick="Pesan('silahkan daftar')">daftar</button>
        </div>
    </nav>
    <br>
    <br>
    <!-- content -->
    <h1> Berikut daftar harga Delman</h1>
    <br>
    <table border="1px solid black">
        <tr>
            <th>NO</th>
            <th>Nama delman</th>
            <th>Harga</th>
            <th><button onclick="Pesan('Pesanan telah dibuat')">Pesan</button></th>
        </tr>
        <tr>
            <th>2</th>
            <th>delman 2</th>
            <th>90</th>
            <th><button onclick="Pesan('Pesanan telah dibuat')">Pesan</button></th>
        </tr>
        <tr>
            <th>3</th>
            <th>delman 3</th>
            <th>900</th>
            <th><button onclick="Pesan('Pesanan telah dibuat')">Pesan</button></th>
        </tr>
        <tr>
            <th>4</th>
            <th>delman 4</th>
            <th>90000</th>
            <th><button onclick="Pesan('Pesanan telah dibuat')">Pesan</button></th>
        </tr>
    </table>
  
   <script src="{{ asset('script2.js') }}"></script>
</body>
</html>