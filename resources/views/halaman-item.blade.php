<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('style.css') }}">

</head>
<body>
    <nav class="nav">
        <div class="nav-kiri">
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">contact</a></li>
            <li><a href="#">about</a></li>
        </ul>
        </div>
        <div class="nav-kanan">
            <button onclick="myFunction('silahkan login')">login</button>
            <button onclick="myFunction('silahkan daftar')">daftar</button>
        </div>
    </nav>
    <br>
   <!-- content -->
   <p>Silahkan klick masing-masing warna di bawah ini</p>
   <br>
   <h1 onclick="myFunction('selamat dtang di content')">hy ini content</h1>
   <br>
   <h2 onclick="myFunction('selamat datang di h2')">hy ini h2</h2>
<br>
   <h3 onclick="myFunction('selamat datang di h3')">hy ini h3</h3>
<br>
   <h4 onclick="myFunction('selamat datang di h4')">hy ini h4</h4>
<br>
   <h5 onclick="myFunction('selamat datang di h5')">hy ini h5</h5>
<br>
   <h6 onclick="myFunction('selamat datang di h6')">hy ini h6</h6>
   <br>
   <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus eius vero libero non laboriosam repellat optio ipsam praesentium aspernatur dignissimos. Officiis, doloribus porro rem totam adipisci aliquam facere aliquid itaque.
   Odio consectetur minus et. Sapiente quas incidunt culpa commodi voluptatem optio eos repellat maxime. Unde dolores, expedita vero fuga assumenda laudantium nihil dignissimos architecto, facere quisquam aliquam, qui inventore natus.
   Tempore vitae voluptatem quo praesentium expedita voluptate at maxime voluptates dolorum beatae perspiciatis a placeat, et alias totam accusamus asperiores esse nemo molestias. Amet fuga cumque neque, nisi eveniet molestias!
   Sed quis sunt quod voluptatem. Placeat, eaque dolorem architecto unde ab repellendus aliquid sunt voluptate odio. Mollitia reiciendis placeat enim in. Iusto ab quo blanditiis, deserunt ipsa vero ea! Perspiciatis.
   Ipsa tempore facere, voluptatibus laborum labore maxime delectus vel consectetur voluptatem cupiditate totam sunt, facilis perspiciatis voluptate perferendis quasi alias accusantium veniam pariatur? Ipsam doloremque, eos a nobis velit magni?
   Sed obcaecati natus sunt quis vitae aut quam distinctio dignissimos architecto nihil rerum sit inventore dolores labore, fuga praesentium voluptates tempora qui maxime explicabo deleniti recusandae quia. Fuga, distinctio doloremque?</p>

   <script src="{{ asset('script.js') }}"></script>
</body>
</html>